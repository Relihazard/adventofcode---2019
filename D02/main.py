#!/usr/bin/env python3

import sys


def solve_part_one(intcode, noun, verb):
    intcode[1] = noun
    intcode[2] = verb
    for opcode in range(0, len(intcode), 4):
        if int(intcode[opcode]) == 1:
            intcode[int(intcode[opcode + 3])] =\
                    int(intcode[int(intcode[opcode + 1])])\
                    + int(intcode[int(intcode[opcode + 2])])
        elif int(intcode[opcode]) == 2:
            intcode[int(intcode[opcode + 3])] =\
                    int(intcode[int(intcode[opcode + 1])])\
                    * int(intcode[int(intcode[opcode + 2])])
        elif int(intcode[opcode]) == 99:
            break
        else:
            break
    return int(intcode[0])


def solve_part_two(intcode, noun, verb):
    for i in range(100):
        for j in range(100):
            if solve_part_one(intcode[:], i, j) == 19690720:
                return 100 * i + j
    return 100 * int(noun) + int(verb)


with open(sys.argv[1]) as f:
    intcode = f.readline().split(',')
    print(solve_part_one(intcode[:], '12', '2'))
    print(solve_part_two(intcode[:], '12', '2'))
