#!/usr/bin/env python3

import math
import sys

total = 0

with open(sys.argv[1]) as f:
    line = f.readline()
    while line:
        while int(line) > 0:
            line = math.floor(int(line) / 3) - 2
            if line <= 0:
                continue
            total += line
        line = f.readline()
print(total)
